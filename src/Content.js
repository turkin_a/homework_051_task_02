import React from 'react';

const Content = props => {
  return (
    <div className="container">
      <div className="items">
        <div className="item1">
          <a href="#" className="ico"><i></i><h4>clean theme</h4></a>
          <p>Ut nec lorem id orci convallis porta. Donec pharetra neque eu velit dictum molestie. Duis porta gravida augue sed viverra. Quisque at nulla leo, non aliquet mi.</p>
          <a href="#" className="more">Read More</a>
        </div>
        <div className="item2">
          <a href="#" className="ico"><i></i><h4>responsive design</h4></a>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi euismod placerat dui et tincidunt. Sed sollicitudin posuere auctor. Phasellus at ultricies nisl. Integer at leo eros.</p>
          <a href="#" className="more">Read More</a>
        </div>
        <div className="item3">
          <a href="#" className="ico"><i></i><h4>fully layerd PSD</h4></a>
          <p>Phasellus lobortis metus non augue sodales volutpat. Vestibulum sit amet nibh eros, hendrerit venenatis est. In vitae nulla nec purus cursus pretium sed id magna.</p>
          <a href="#" className="more">Read More</a>
        </div>
        <div className="item4">
          <a href="#" className="ico"><i></i><h4>ready for coding</h4></a>
          <p>Vivamus convallis feugiat mauris sed posuere. Nam rutrum, quam quis euismod commodo, elit est porta quam, non placerat eros neque porta ante. Fusce quis odio urna.</p>
          <a href="#" className="more">Read More</a>
        </div>
      </div>
    </div>
  );
};

export default Content;