import React from 'react';

const Header = props => {
  return (
    <header>
      <div className="container">
        <div className="logo">
          <a href="#" title="BisLite"><img src="img/logo-bis.png" height="37" width="148" alt="BisLite" /></a>
        </div>
        <nav className="nav-header">
          <ul>
            <li><a href="#">home</a></li>
            <li><a href="#">about us</a></li>
            <li><a href="#">services</a></li>
            <li><a href="#">portfolio</a></li>
            <li><a href="#">blog</a></li>
            <li><a href="#">contact us</a></li>
          </ul>
        </nav>
      </div>
    </header>
  );
};

export default Header;