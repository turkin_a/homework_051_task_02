import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './Header'
import Promo from './Promo'
import Content from './Content'
import Folio from './Folio'
import Footer from './Footer'

const App = props => {
  return (
    <div className='App'>
      <Header />
      <Promo />
      <Content />
      <Folio />
      <Footer />
    </div>
  );
};

export default App;