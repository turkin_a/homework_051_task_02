import React from 'react';

const Promo = props => {
  return (
    <div className="promo">
      <div className="container">
        <h2>We design clean, crisp &amp; memorable icons</h2>
      </div>
    </div>
  );
};

export default Promo;