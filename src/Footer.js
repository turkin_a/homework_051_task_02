import React from 'react';

const Footer = props => {
  return (
    <div className="container">
      <p>&copy; Copyright 2012 - BisLite Inc. All rights reserved. Some free icons used here are created by Brankic1979.com.</p>
      <p>Client Logos are copyright and trademark of the respective owners / companies.</p>
    </div>
  );
};

export default Footer;