import React from 'react';

const Folio = props => {
  return (
    <div className="resources">
      <div className="container">
        <h4 className="latest-works">latest works</h4>
        <div className="works">
          <a href="#" className="work"><img src="img/img-works1.jpg" height="160" width="220" alt="" /></a>
          <a href="#" className="work"><img src="img/img-works2.jpg" height="160" width="220" alt="" /></a>
          <a href="#" className="work"><img src="img/img-works3.jpg" height="160" width="220" alt="" /></a>
          <a href="#" className="work"><img src="img/img-works4.jpg" height="160" width="220" alt="" /></a>
        </div>
        <div className="download">
          <a href="#" className="btn">download PSD</a>
        </div>
        <h4 className="testimonials">testimonials</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ut nulla sapien, at aliquam erat. Sed vitae massa tellus. Aliquam commodo aliquam metus, sed iaculis nibh tempus id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam nec nisi in nisl euismod fringilla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ut nulla sapien, at aliquam erat. Sed vitae massa tellus. Aliquam commodo aliquam metus, sed iaculis nibh tempus id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam nec nisi in nisl euismod fringilla.</p>
        <p className="author">John Travis, CEO, DomainName.com</p>
      </div>
    </div>
  );
};

export default Folio;